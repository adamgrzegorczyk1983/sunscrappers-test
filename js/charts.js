	  
	  // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // przygotowujemy dane
       var data = google.visualization.arrayToDataTable([
          ['Date', 'A', 'B', 'B', 'B'],
          ['25.01.2014',  34,      34,	9,	2],
          ['25.01.2014',  34,      34,	7,	3],
          ['25.01.2014',  36,      24,	0,	7],
          ['25.01.2014',  36,      16,	8,	0]
        ]);
	
		
        // ustawiamy opcje
        var options = {
						'chartArea':{left:20,top:25,width:"100%",height:"80%"},
						'enableInteractivity':true,
						'fontSize':8,
                        'height':230,
						'legend':'none',
						
						'vAxis': {'minorGridlines': {'color':'#f2f1f1', 'count': 10}, 'gridlines': {'color':'#e9e9e9'}}
						};
		
		//wrzucamy w funkcje dzieki czemu możemy odpalic przy każdej zmianie rozmiaru okna
		function resize(){
		
        // genereujemy i rysujemy wykre w oparciu o zadane dane i opcje
        var chart = new google.visualization.LineChart(document.getElementById('chart_1'));
        chart.draw(data, options);
		var chart = new google.visualization.LineChart(document.getElementById('chart_2'));
        chart.draw(data, options);
		var chart = new google.visualization.LineChart(document.getElementById('chart_3'));
        chart.draw(data, options);
		var chart = new google.visualization.LineChart(document.getElementById('chart_4'));
        chart.draw(data, options);	
		var chart = new google.visualization.LineChart(document.getElementById('chart_5'));
        chart.draw(data, options);			
		};
		
		//odpalamy pierwotne wyrysowanie wykresów, i podpinamy funkcje pod zdarzenie zmiany rozmiaru okna
		resize();
		window.onresize=resize;
		
	
		
      }

	  
	  